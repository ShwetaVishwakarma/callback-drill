module.exports.listBelongToBoardId = (boardID, listsData, callback) => {
    setTimeout(() => {
        // Your code here
        if (typeof callback !== "function") {
            console.error("Callback is not a Function");
        }
        else if (typeof boardID === "string" && typeof listsData === "object") {
            try {
                if (listsData.hasOwnProperty(boardID)) {
                    callback(null, listsData[boardID]);
                } else {
                    let err = new Error("Data Not Found");
                    callback(err);
                }
            } catch (err) {
                callback(err)
            }
        } else {
            let err = new Error("Invalid Data");
            callback(err);
        }
    }, 2 * 1000);
}