module.exports.Problem4 = (obj) => {

    setTimeout(() => {
        const { boardName, listName, boardsData, listsData, cardsData, getBoardsInformation, listBelongToBoardId, cardBelongToParticularList } = obj;
        console.log(typeof listName);
        if (typeof boardName !== "string" && typeof listName !== "string") {
            console.log("Invalid data");
        } else {
            // Your code here
            boardsData.forEach(boardData => {
                getBoardsInformation(boardData.id, boardsData, (err, data) => {
                    if (err) {
                        console.log(err.message);
                    } else if (data.name === boardName) {
                        listBelongToBoardId(data.id, listsData, (err, data) => {
                            if (err) {
                                console.log(err);
                            } else {
                                data.forEach(listData => {
                                    console.log(listData.name);
                                    if (listData.name === listName) {
                                        cardBelongToParticularList(listData.id, cardsData, (err, data) => {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                data.forEach(resultElement => {
                                                    simultaneouslyPrint(resultElement)
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        })
                    }
                })
            });
        }

    }, 2 * 1000);
}
var offset = 0;
simultaneouslyPrint = (recieveElement) => {
    setTimeout(() => {
        console.log(recieveElement);
    }, 2000 + offset);
    offset += 2000;
}
