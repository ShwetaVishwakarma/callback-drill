module.exports.Problem6 = (obj) => {
    setTimeout(() => {
        // Your code here
        const { boardName, boardsData, listsData, cardsData, getBoardsInformation, listBelongToBoardId, cardBelongToParticularList } = obj

        boardsData.forEach(boardData => {
            getBoardsInformation(boardData.id, boardsData, (err, data) => {
                if (err) {
                    console.log(err.message);
                } else if (data.name === boardName) {
                    //console.log(data.id);
                    listBelongToBoardId(data.id, listsData, (err, data) => {
                        if (err) {
                            console.log(err.message);
                        }
                        else {
                            data.forEach((listData) => {
                                cardBelongToParticularList(listData.id, cardsData, (err, data) => {
                                    if (err) {
                                        console.log(err.message);
                                    } else {
                                        //console.log(data);
                                        data.forEach(resultElement => {
                                            //console.log(resultElement);
                                            simultaneouslyPrint(resultElement)
                                        })
                                    }
                                })
                            })
                        }
                    })
                }
            })
        });
    }, 2 * 1000);
}
var offset = 0;
simultaneouslyPrint = (recieveElement) => {
    setTimeout(() => {
        console.log(recieveElement);
    }, 2000 + offset);
    offset += 2000;
}
