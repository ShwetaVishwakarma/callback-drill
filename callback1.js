module.exports.getBoardsInformation = (boardID, boardData, callback) => {
    setTimeout(() => {
        // Your code here
        // console.log(arguments.length);
         //console.log(Array.isArray(boardData));
        if (typeof callback !== "function") {
            console.error("Callback is not a Function");
        }
        else if (typeof boardID === "string" && Array.isArray(boardData) && boardData.length !== 0) {
            try {
                let matchData = boardData.find((particularData) => {
                    if (particularData.id === boardID) {
                        return true;
                    }
                })
                //console.log(matchData);
                if (matchData) {
                    callback(null, matchData);
                } else {
                    let err = new Error("Data Not Found");
                    callback(err);
                }
            } catch (err) {
                callback(err)
            }
        } else {
            let err = new Error("Invalid Data");
            callback(err);
        }

    }, 2 * 1000);
}