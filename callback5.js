module.exports.Problem5 = (obj) => {
    setTimeout(() => {
        // Your code here
        
        const { boardName, listNames, boardsData, listsData, cardsData, getBoardsInformation, listBelongToBoardId, cardBelongToParticularList } = obj
        boardsData.forEach(boardData => {
            getBoardsInformation(boardData.id, boardsData, (err, data) => {
                if (err) {
                    console.log(err.message);
                } else if (data.name === boardName) {
                    //console.log(data.id);
                    listBelongToBoardId(data.id, listsData, (err, data) => {
                        if (err) {
                            console.log(err);
                        } else {
                            let lists = data.filter((dataList) => listNames.includes(dataList.name))
                            if (lists) {
                                lists.forEach((listData) => {
                                    cardBelongToParticularList(listData.id, cardsData, (err, data) => {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            //console.log(data);
                                            data.forEach(resultElement => {
                                                simultaneouslyPrint(resultElement)
                                            })
                                        }
                                    })
                                })
                            }
                        }
                    })
                }
            })
        });
    }, 2 * 1000);
}

var offset = 0;
simultaneouslyPrint = (recieveElement) => {
    setTimeout(() => {
        console.log(recieveElement);
    }, 2000 + offset);
    offset += 2000;
}
