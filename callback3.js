module.exports.cardBelongToParticularList = (listID, cardData, callback) => {
    setTimeout(() => {
        // Your code here
        if (typeof callback !== "function") {
            console.error("Callback is not a Function");
        }
        if (typeof listID === "string" && typeof cardData === "object") {
            try {
                if (cardData.hasOwnProperty(listID)) {
                    callback(null, cardData[listID]);
                } else {
                    let err = new Error("Data Not Found");
                    callback(err);
                }
            } catch (err) {
                callback(err);
            }
        } else {
            let err = new Error("Invalid Data");
            callback(err);
        }
    }, 2 * 1000);
}