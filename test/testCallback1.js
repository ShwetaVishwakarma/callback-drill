const path = require("path");
const { getBoardsInformation } = require("../callback1.js")
const boardsData = require(path.join(__dirname, "../Data/boards.json"));
function callback(err,data) {
    if (err) {
        console.error('There was an error:', err.message);
    } else {
        console.log(data);
    }
}
getBoardsInformation(boardsData[Math.floor(Math.random() * boardsData.length)].id, boardsData, callback);

// Invalid Id
getBoardsInformation(
    "mcu453edgg", 
    boardsData,
    callback
);
// Invalid boards data file name pass
getBoardsInformation(
    "mcu453edgg", 
    [],
    callback
);
// Not passing callback
getBoardsInformation(
    "mcu453edgg", 
    boardsData
);