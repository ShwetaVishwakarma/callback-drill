const path = require("path");
const { Problem4 } = require("../callback4.js")
const boardsData = require(path.join(__dirname, "../Data/boards.json"));
const listsData = require(path.join(__dirname, "../Data/lists.json"));
const cardsData = require(path.join(__dirname, "../Data/cards.json"));
const { getBoardsInformation } = require("../callback1.js")
const { listBelongToBoardId } = require("../callback2.js")
const { cardBelongToParticularList } = require("../callback3.js")


let obj= {
    boardName : "Thanos",
    listName :  "Mind",
    boardsData : boardsData,
    listsData : listsData,
    cardsData :cardsData,
    getBoardsInformation: getBoardsInformation,
    listBelongToBoardId : listBelongToBoardId,
    cardBelongToParticularList: cardBelongToParticularList
}

Problem4(obj);