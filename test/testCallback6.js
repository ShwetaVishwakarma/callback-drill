const path = require("path");
const { Problem6 } = require("../callback6.js")
const boardsData = require(path.join(__dirname, "../Data/boards.json"));
const listsData = require(path.join(__dirname, "../Data/lists.json"));
const cardsData = require(path.join(__dirname, "../Data/cards.json"));
const { getBoardsInformation } = require("../callback1.js")
const { listBelongToBoardId } = require("../callback2.js")
const { cardBelongToParticularList } = require("../callback3.js")

let obj= {
    boardName : "Thanos",
    boardsData : boardsData,
    listsData : listsData,
    cardsData :cardsData,
    getBoardsInformation: getBoardsInformation,
    listBelongToBoardId : listBelongToBoardId,
    cardBelongToParticularList: cardBelongToParticularList
}

Problem6(obj);