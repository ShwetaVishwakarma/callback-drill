const path = require("path");
const { cardBelongToParticularList } = require("../callback3.js")
const cardsData = require(path.join(__dirname, "../Data/cards.json"));

function callback(err,data) {
    if (err) {
        console.error('There was an error:', err.message);
    } else {
        console.log(data);
    }
}

cardBelongToParticularList("azxs123", cardsData, callback);

//Invalid Id
cardBelongToParticularList("abc122dccc",
cardsData,
callback
)

//Invalid listData pass
cardBelongToParticularList("mcu453ed",
[],
callback
)

//Invalid arguments pass
cardBelongToParticularList("mcu453ed",
cardsData
)