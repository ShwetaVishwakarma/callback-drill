const path = require("path");
const { listBelongToBoardId } = require("../callback2.js")
const boardsData = require(path.join(__dirname, "../Data/boards.json"));
const listsData = require(path.join(__dirname, "../Data/lists.json"));

function callback(err,data) {
    if (err) {
        console.error('There was an error:', err.message);
    } else {
        console.log(data);
    }
}

listBelongToBoardId(boardsData[Math.floor(Math.random() * boardsData.length)].id, listsData, callback);

//Invalid Id
listBelongToBoardId("abc122dccc",
listsData,
callback
)

//Invalid listData pass
listBelongToBoardId("mcu453ed",
[],
callback
)

//Invalid arguments pass
listBelongToBoardId("mcu453ed",
listsData
)